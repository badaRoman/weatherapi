<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class WeatherAPIController extends Controller
{
    
    public static function getHourlyData(){
        $response = json_decode(Http::get('http://api.openweathermap.org/data/2.5/weather?id='.env('WEATHER_API_NAME').'&appid='.env('WEATHER_API_KEY')));
        $newTemperatureRecord = new \App\Models\HourlyTemperature(['temperature'=>$response->main->temp]);
        $newTemperatureRecord->save();
        return $newTemperatureRecord;
    }
    
}
