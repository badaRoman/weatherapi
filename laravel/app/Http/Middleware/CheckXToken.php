<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckXToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!$request->hasHeader('x-token') || strlen($request->header('x-token')) != 32) {
            abort(404);
        }        
        return $next($request);
    }
}
