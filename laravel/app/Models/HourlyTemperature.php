<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HourlyTemperature extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'temperature',
    ];
    protected $hidden = [
        'updated_at',
        'id'
    ];
}
