<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

/*Route::get('/update', function () {
    $record = \App\Http\Controllers\WeatherAPIController::getHourlyData();
    return $record;
});*/

Route::get('/show', function () {
    $today = (new DateTime())->format('Y-m-d');
    $tomorrow = (new DateTime())->modify('+1 day')->format('Y-m-d');
    $temperatures = \App\Models\HourlyTemperature::where('created_at', '>=', $today)
        ->where('created_at', '<', $tomorrow)
        ->orderBy('created_at')
        ->take(24)
        ->get();    
    return $temperatures;
    $temperatures = count($temperatures) == 0 ?  'No data Found' : json_encode(['daily_temperatures' => $temperatures]) ;
})->middleware('x_token');

Route::get('/show/{date}', function ($date) {
    $date = isset($date) && !empty($date) ? $date : 'now';
    $today = (new DateTime($date))->format('Y-m-d');
    $tomorrow = (new DateTime($date))->modify('+1 day')->format('Y-m-d');
    $temperatures = \App\Models\HourlyTemperature::where('created_at', '>=', $today)
        ->where('created_at', '<', $tomorrow)
        ->orderBy('created_at')
        ->take(24)
        ->get();
    $temperatures = count($temperatures) == 0 ?  'No data Found' : json_encode(['daily_temperatures' => $temperatures]) ;
    return $temperatures;
})->middleware('x_token');
